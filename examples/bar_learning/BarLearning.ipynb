{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bar Learning problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The implementation of the bar learning problem is located in the\n",
    "`examples/bar_learning` folder. The bar learning problem describes the\n",
    "process of learning receptive fields on an artificial input pattern.\n",
    "Images consisting of independent bars are used. Those images are\n",
    "generated as following: an 8\\*8 image can filled randomly by eight\n",
    "horizontal or vertical bars, with a probability of 1/8 for each.\n",
    "\n",
    "These input images are fed into a neural population, whose neurons\n",
    "should learn to extract the independent components of the input\n",
    "distribution, namely single horizontal or vertical bars.\n",
    "\n",
    "If you have `pyqtgraph` installed, you can simply try the network by\n",
    "typing:\n",
    "\n",
    "~~~\n",
    "python BarLearning.py\n",
    "~~~\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model consists of two populations `Input` and `Feature`. The size of\n",
    "`Input` should be chosen to fit the input image size (here 8\\*8). The\n",
    "number of neurons in the `Feature` population should be higher than the\n",
    "total number of independent bars (16, we choose here 32 neurons). The\n",
    "`Feature` population gets excitory connections from `Input` through an\n",
    "all-to-all connection pattern. The same pattern is used for the\n",
    "inhibitory connections within `Feature`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining the neurons and populations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ANNarchy 4.6 (4.6.0b) on linux (posix). \n"
     ]
    }
   ],
   "source": [
    "from ANNarchy import *\n",
    "clear()\n",
    "\n",
    "#setup(paradigm=\"cuda\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Input population:**\n",
    "\n",
    "The input pattern will be clamped into this population by the main\n",
    "loop for every trial, so we need just an empty neuron at this\n",
    "point:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "InputNeuron = Neuron(parameters=\"r = 0.0\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The trick here is to declare `r` as a parameter, not a variable: its\n",
    "value will not be computed by the simulator, but only set by external\n",
    "input. The `Input` population can then be created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Input = Population(geometry=(8, 8), neuron=InputNeuron)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Feature population:**\n",
    "\n",
    "The neuron type composing this population sums up all the excitory\n",
    "inputs gain from `Input` and the lateral inhibition within `Feature`.\n",
    "\n",
    "$$\\tau \\frac {dr_{j}^{\\text{Feature}}}{dt} + r_{j}^{Feature} = \\sum_{i} w_{ij} \\cdot r_{i}^{\\text{Input}}  - \\sum_{k, k \\ne j} w_{kj} * r_{k}^{Feature}$$\n",
    "\n",
    "could be implemented as the following:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "LeakyNeuron = Neuron(\n",
    "    parameters=\"\"\" \n",
    "        tau = 10.0 : population\n",
    "    \"\"\",\n",
    "    equations=\"\"\"\n",
    "        tau * dr/dt + r = sum(exc) - sum(inh) : min=0.0\n",
    "    \"\"\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The firing rate is restricted to positive values with the `min=0.0`\n",
    "flag. The population is created in the following way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Feature = Population(geometry=(8, 4), neuron=LeakyNeuron)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We give it a (8, 4) geometry for visualization only, it does not\n",
    "influence computations at all."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining the synapse and projections"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both feedforward (`Input` $\\rightarrow$ `Feature`) and lateral\n",
    "(`Feature` $\\rightarrow$ `Feature`) projections are learned using the\n",
    "Oja learning rule (a regularized Hebbian learning rule ensuring the sum\n",
    "of all weights coming to a neuron is constant). Only some parameters will\n",
    "differ between the projections.\n",
    "\n",
    "$$\\tau \\frac{dw_{ij}}{dt} = r_{i} * r_{j} - \\alpha * r_{j}^{2} * w_{ij}$$\n",
    "\n",
    "where $\\alpha$ is a parameter defining the strength of the\n",
    "regularization, $r_i$ is the pre-synaptic firing rate and $r_j$ the\n",
    "post-synaptic one. The implementation of this synapse type is\n",
    "straightforward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Oja = Synapse(\n",
    "    parameters=\"\"\" \n",
    "        tau = 2000.0 : postsynaptic\n",
    "        alpha = 8.0 : postsynaptic\n",
    "        min_w = 0.0 : postsynaptic\n",
    "    \"\"\",\n",
    "    equations=\"\"\"\n",
    "        tau * dw/dt = pre.r * post.r - alpha * post.r^2 * w : min=min_w\n",
    "    \"\"\"\n",
    ")  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this network we need to create two projections, one excitory between\n",
    "the populations `Input` and `Feature` and one inhibitory within the\n",
    "`Feature` population itself:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<ANNarchy.core.Projection.Projection at 0x7fe7a639e2e8>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ff = Projection(\n",
    "    pre=Input, \n",
    "    post=Feature, \n",
    "    target='exc', \n",
    "    synapse = Oja    \n",
    ")\n",
    "ff.connect_all_to_all(weights = Uniform(-0.5, 0.5))\n",
    "                     \n",
    "lat = Projection(\n",
    "    pre=Feature, \n",
    "    post=Feature, \n",
    "    target='inh', \n",
    "    synapse = Oja\n",
    ")\n",
    "lat.connect_all_to_all(weights = Uniform(0.0, 1.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The two projections are all-to-all and use the `Oja` synapse type. They\n",
    "only differ by the parameter `alpha` (lower in `lat`) and\n",
    "the fact that the weights of `ff` are allowed to be negative\n",
    "(so we set the minimum value to -10.0):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "ff.min_w = -10.0\n",
    "lat.alpha = 0.3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting inputs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the network is defined, one has to specify how inputs are fed into\n",
    "the `Input` population. A simple solution is to define a method that\n",
    "sets the firing rate of `Input` according to the specified probabilities\n",
    "every time it is called, and runs the simulation for 50 ms:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def trial():\n",
    "    # Reset the firing rate for all neurons\n",
    "    Input.r = 0.0\n",
    "    # Clamp horizontal bars randomly\n",
    "    for h in range(Input.geometry[0]):\n",
    "        if np.random.random() < 1.0/ float(Input.geometry[0]):\n",
    "            Input[h, :].r = 1.0\n",
    "    # Clamp vertical bars randomly\n",
    "    for w in range(Input.geometry[1]):\n",
    "        if np.random.random() < 1.0/ float(Input.geometry[1]):\n",
    "            Input[:, w].r = 1.0\n",
    "    # Simulate for 50ms\n",
    "    simulate(50.)\n",
    "    # Return firing rates and receptive fields for visualization\n",
    "    return Input.r, Feature.r, ff.receptive_fields()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can use here a single value or a Numpy array (e.g.\n",
    "`np.zeros(Input.geometry))`) to reset activity in Input, it does not matter.\n",
    "\n",
    "For all possible horizontal bars, a decision is then made whether the\n",
    "bar should appear or not, in which case the firing rate of the\n",
    "correspondng neurons is set to 1.0:\n",
    "\n",
    "``` {.python}\n",
    "    for h in range(Input.geometry[0]):\n",
    "        if np.random.random() < 1.0/ float(Input.geometry[0]):\n",
    "            Input[h, :].r = 1.0\n",
    "```\n",
    "\n",
    "`Input[h, :]` is a PopulationView, i.e. a group of neurons defined by\n",
    "the sub-indices (here the row of index `h`). Their attributes, such as\n",
    "`r`, can be accessed as if it were a regular population. The same is\n",
    "done for vertical bars."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running the simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the method for setting inputs is defined, the simulation can be\n",
    "started. A basic approach would be to define a `for` loop where the\n",
    "`trial()` method is called repetitively:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "compile()\n",
    "\n",
    "for t in range(1000):\n",
    "    input_r, feature_r, weights = trial()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAABHsAAAF3CAYAAAA1q0cyAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJzs3Xu4XXV97/vPx9wTck8IuUFEUKRYQbPRPnisRXQDWtFq\nLe5Tqxabnh4p0tpdEetl+7Td1N1K7dYjB4Et1hsURSiNt6qInF3QFeRqQJMYSEjIBXK/B77njzmi\nkzW+M+u3si5zrpn363nyZK3vHJfvuMy15vrNMT/DESEAAAAAAAB0h+e0uwEAAAAAAAAMHgZ7AAAA\nAAAAugiDPQAAAAAAAF2EwR4AAAAAAIAuwmAPAAAAAABAF2GwBwAAAAAAoIsw2AMAAAAAANBFGOzB\niGB7te1zhngdH7X9haFcBwAAAAAAQ43BHgAAAAAAgC7CYA9GFNvvtH2n7b+3vcX2L2yf1/T47bb/\nu+0f2d5m+xbbM6rHXmV7ba/lrbZ9ju1zJV0u6fds77R93/BuGQAAAAAAg4PBHoxEL5P0iKRZkj4u\n6Vrbbnr8DyT9oaR5kg5K+qe+FhgR35T0t5JuiIhjIuLFg941AAAAAADDgMEejESPRsRnI+JpSddL\nmitpTtPj/xwRD0bELkkfkvRW26Pa0SgAAAAAAMONwR6MRE8c+iIidldfHtP0+Jqmrx+VNEaNq4AA\nAAAAAOh6DPagGy1s+vp4SQckbZa0S9LEQw9UV/vMbpo2hqU7AAAAAACGEIM96Ea/b/tU2xMlfUzS\nTdVHvn4mabzt19keI+mvJI1rmm+DpEW2eV4AAAAAAEYs/qhFN/pnSZ9T4+Ne4yVdIkkRsU3S/y3p\nGkmPq3GlT/Pduf6l+v9J2/cMV7MAAAAAAAwmR/DJFXQP27dL+kJEXNPuXgAAAAAAaAeu7AEAAAAA\nAOgiDPYAAAAAAAB0ET7GBQAAAAAA0EW4sgcAAAAAAKCLMNgDAAAAAADQRUYPxUJnzZoVixYtGopF\nA8CItnr1am3evNnt7gMAAABA9xqSwZ5Fixapp6dnKBYNACPa4sWL290CAAAAgC7Hx7gAAAAAAAC6\nCIM9AAAAAAAAXYTBHgAAAAAAgC7CYA8AAAAAAEAXYbAHAAAAAACgizDYAwAAAAAA0EUY7AEAAAAA\nAOgiDPYAAAAAAAB0EQZ7AAAAAAAAugiDPQAAAAAAAF2EwR4AAAAAAIAuwmAPAAAAAABAF2GwBwAA\nAAAAoIsw2AMAAAAAANBFGOwBAAAAAADoIkWDPbbPtf2I7RW2LxvqpgAAAAAAAHBk+hzssT1K0qcl\nnSfpVElvs33qUDcGAAAAAACA/iu5sudMSSsiYlVE7Jf0FUkXDG1bAAAAAAAAOBIlgz3zJa1p+n5t\nVXsW20ts99ju2bRp02D1BwAAAAAAgH4oGexxUotaIeLqiFgcEYtnz5498M4AAAAAAADQbyWDPWsl\nLWz6foGkdUPTDgAAAAAAAAaiZLDnx5JOtv1c22MlXSjp1qFtCwAAAAAAAEdidF8TRMRB2xdL+pak\nUZKui4iHhrwzAAAAAAAA9Fufgz2SFBFLJS0d4l4AAAAAAAAwQCUf4wIAAAAAAMAIwWAPAAAAAABA\nF2GwBwAAAAAAoIsw2AMAAAAAANBFGOwBAAAAAADoIgz2AAAAAAAAdBEGewAAAAAAALoIgz0AAAAA\nAABdhMEeAAAAAACALsJgDwAAAAAAQBdhsAcAAAAAAKCLMNgDAAAAAADQRRjsAQAAAAAA6CIM9gAA\nAAAAAHQRR8TgL9Qe/IWiIw3F+TPYbLe7BeBZIoKTEgAAAMCQ4coeAAAAAACALsJgDwAAAAAAQBdh\nsAcAAAAAAKCLMNgDAAAAAADQRRjsAQAAAAAA6CIM9gAAAAAAAHQRBnsAAAAAAAC6CIM9AAAAAAAA\nXYTBHgAAAAAAgC7CYA8AAAAAAEAXYbAHAAAAAACgizDYAwAAAAAA0EUY7AEAAAAAAOgiDPYAAAAA\nAAB0EQZ7AAAAAAAAugiDPQAAAAAAAF2EwR4AAAAAAIAu0udgj+3rbG+0/eBwNAQAAAAAAIAjV3Jl\nz+cknTvEfQAAAAAAAGAQ9DnYExF3SHpqGHoBAAAAAADAAI0erAXZXiJpyWAtDwAAAAAAAP3niOh7\nInuRpNsi4rSihdp9LxRdoeT8aTfb7W4BeJaI4KQEAAAAMGS4GxcAAAAAAEAXYbAHAAAAAACgi5Tc\nev3Lkv5D0gtsr7V90dC3BQAAAAAAgCNRlNnT74WS2XPUILMH6D8yewAAAAAMJT7GBQAARgTbq23v\nsb2z6d+8ASzvVbbXDmaPAAAAnYDBHgAAMJL8dkQc0/RvXbsasT26XesGAAy+6k2EE4dguX9ie0O1\n/Jml6+nrTQnbn7P914PbLboFgz0AAGBEs/1y2//b9lbb99l+VdNj77K93PYO26ts/3FVnyTpG5Lm\nNV8l1PuFc+8X2tXVRe+3fb+kXbZHV/N91fYm27+wfcnwbT0AdL5eV2Y+Uf2sPabNPd1u+93NtepN\nhFWDvJ4xkj4h6bXV8p8civUAvTHYAwAARizb8yX9m6S/ljRD0l9I+qrt2dUkGyW9XtIUSe+SdKXt\nl0TELknnSVp3BFcJvU3S6yRNk/SMpH+VdJ+k+ZJeLelS2/95UDYQALrHb0fEMZJOl3SGpA+0uZ/h\nMkfSeEkPtbsRHF0Y7AEAACPJ16sreLba/rqk35e0NCKWRsQzEfEdST2SzpekiPi3iFgZDT+Q9G1J\n/8cAe/iniFgTEXsk/SdJsyPiYxGxv3qn9rOSLhzgOgCgK0XEE5K+pcagjyTJ9jjbf2/7serjTlfZ\nntD0+AW277W93fZK2+dW9am2r7W93vbjtv/a9qjqsXfa/v9s/0/b22w/bPvV1WN/o8bvgk9VVxt9\nqqqH7ZOqK0afOLSs6rE3VVd1yvZzbF9W9fKk7Rttz+i9rbafL+mR6tuttr/XvJ6Sbe+1vDNs31Nd\nrXqDGoNIhx6bZfu26vfjU7Z/aJu/949iHHwAADCSvDEiplX/3ijpBEm/2zQAtFXSKyTNlSTb59m+\nq3rhu1WNQaBZA+xhTdPXJ6jxUbDm9V+uxju5AIBebC9Q48rKFU3lv5P0fDUGgE5S40rJD1fTnynp\n85L+qxpXVL5S0upqvuslHazmOUPSayU1fzTrZZJWqfFz/yOSvmZ7RkR8UNIPJV1cXdl5cXOPEXGX\npF2Szm4q/xdJX6q+vkTSGyX9pqR5krZI+nTvbY2In0n6terbaRFxdu9pDrftzWyPlfR1Sf+sxpWs\n/yLpzU2TvE/SWkmz1fgddLmkzr91MoYMgz0AAGAkWyPpn5sGgKZFxKSIuML2OElflfT3kuZExDRJ\nSyW5mjd7EbxL0sSm749Lpmmeb42kX/Ra/+SIOH/AWwYA3eXrtneo8XNzoxqDL7JtSX8k6c8i4qmI\n2CHpb/WrKyQvknRdRHynuoLz8Yh42PYcNQaNLo2IXRGxUdKVevaVlRsl/WNEHIiIG9S4yuZ1hf1+\nWY2P7cr2ZDXeLPhy9dgfS/pgRKyNiH2SPirpLe5ncH/Btjd7uaQxTdtzk6QfNz1+QI03Ok6oHv9h\nRDDYcxRjsAcAAIxkX5D027b/s+1RtsdXocoLJI2VNE7SJkkHbZ+nxru+h2yQNNP21KbavZLOtz3D\n9nGSLu1j/T+StL0KbZ5Q9XCa7f80aFsIAN3hjRExWdKrJJ2iX11lOVuNQfZlTVdIfrOqS9JCSSuT\n5Z2gxuDH+qb5/l9JxzZN83ivAY9H1bgSp8SXJP1O9cbB70i6JyIebVr3zU3rXS7pafX/qs6+tr3Z\nvBbbc8j/UONqqW+7cUOCy/rZC7oMgz0AAGDEiog1ki5Q43L1TWq8Y/xfJT2neof0Ekk3qnGJ/X+R\ndGvTvA+r8S7tqupF9jw1Lo+/T42PCHxb0g19rP9pSb+txuX3v5C0WdI1kqYebj4AOFpV+WmfU+Oq\nS6nxc3OPpF9rukJyahXmLDV+rj8vWdQaSfskzWqab0pE/FrTNPOrq2cOOV7SoTD+w171EhE/VWMw\n5Tw9+yNch9Z9Xq+rOsdHxON9bH5vfW17s/UttudQvzsi4n0RcaIav5f+/FBGEY5ODPYAAIARISIW\nRcS/J/W7I+I3I2JGRMyOiNdFxGPVY5+OiDnVC+i3R8SFEfFXTfP+YUTMrB5fFxF7I+L3qj8Yfj0i\nroyIBYfroZrvbRFxXERMj4iXZ30CAH7pHyW9xvbpEfGMGsH2V9o+VmrcabHprobXSnqX7VdXwcjz\nbZ8SEevVGJT/B9tTqseeZ/s3m9ZzrKRLbI+x/buSXqjGx3mlxtWdJ/bR55fUeNPglWpk5BxylaS/\nsX1C1e9s2xf0dycUbHuz/1Ajn+gS26Nt/46kMw89aPv1Vbi0JW1X40qjp/vbE7oHgz0AAAAAgGET\nEZvUCF3+UFV6vxofQbrL9nZJ/y7pBdW0P5L0LjXyeLZJ+oEaH6OSpD9Q4yO7P1XjCs6bVAX0V+6W\ndLIaV9D8jaS3RMST1WOfVCNnZ4vtf2rR6pfV+NjZ9yJic1P9k2pcKfrtKofoLjXCoI9Ey21vFhH7\n1fg42TvV2Nbfk/S1pklOrubdqcbA0P8TEbcfYU/oAh6KzCbbBEEdJUZC5tezr3QE2i8iOv6krG4f\neoOkRWp8nOWtEbElme5pSQ9U3z4WEW8Yrh4BAABasf1OSe+OiFe0uxegHbiyBwCQuUzSdyPiZEnf\nrb7P7ImI06t/DPQAAAAAHYDBHgBA5gJJ11dfXy/pjW3sBQAAAEA/DMnHuBYvXhw9PT2DvlwAGOkW\nL16snp6ekfAxrq0RMa3p+y0RMT2Z7qAat6o+KOmKiPj6MLYJAAAAIDG63Q0AANrD9r9LOi556IP9\nWMzxEbHO9omSvmf7gYhYmaxriaQl1bcv7X+36G327NntbqFPmzZtancLGEYvetGL2t1Cnx544IG+\nJ2q/zRHR+U/wfrJ9rhqhtqMkXRMRV7S5JQDoagz2AMBRKiLOafWY7Q2250bEettzJW1ssYx11f+r\nbN8u6QxJtcGeiLha0tXVsjs/2X0EePOb39zuFvp01VVXtbsFDKPbbrut3S306YQTTuh7ovZ7tN0N\nDDbboyR9WtJrJK2V9GPbt0bET7PpJ02aFNOmTXtWbfTosj9bsk8tZDfrGDduXK22f//+Wu2ZZ54p\nWm+rabN1D/bNQ0rXm8mmy/bhc55Tlv6RzVt6TFrVs+0bO3ZsrbZv376SFov3zUD2Q+l+Ld03pftF\nynvM1jNq1Kh0/pJ5S3vM5n366frd4LPn98GDB2u17Lhnz9uBPsey+bO+S/f1QD5NlfWyZs2aojcF\nGOwBAGRulfQOSVdU/9/SewLb0yXtjoh9tmdJOkvSx4e1SwDASHCmpBURsUqSbH9FjWy4dLBn2rRp\nes973vOs2syZM2vTZX/sZn+QZX/UPu95z6vVHn20Ps5WOoAgSXv27Clad/aHbenAQvaH3+7du2u1\nMWPGFK0jmy77I3vSpElFyztw4EDR8lr9MT5hwoRabefOnbXawoULa7VVq1aly+wt2//Zccq2Jesv\nO+eyQYlsumygIjsmWc/ZcZekiRMnFq176tSp6fy9lfZYOvCxbdu2Wi17fm/cWH+f8fjjj6/VHn/8\n8VotO55Zf9l+kfLB4C1bajel1eTJk2u10udAtu5sf2Xn0nvf+96iNwUIaAYAZK6Q9BrbP1fjndgr\nJMn2YtvXVNO8UFKP7fskfV+NzJ70hTsA4Kg2X9Kapu/XVrVfsr3Edo/tnl27dg1rcwDQjbiyBwBQ\nExFPSnp1Uu+R9O7q6/8tqfNDOgAA7ZZdxvGst7CbP+47f/58Pu4LAAPEYA8AAACAobRWUvPnbhZI\nWtefBZRe7ZN9XCKTfTQl+xjW3r17i5YnleeYDCRPJPsoSunHvbKP3mQfEck+ApNNV5pnVNrz4eq9\nZdtX+vG4bPuy5WXTlZ5fpRktpR9F7I/58+fXatnHkrJjmn0EKduv2XHK5s32Q/ZxqNL9WnrOZevN\nnvOtcosGO3cnO6al29Kf3LDe+BgXAAAAgKH0Y0kn236u7bGSLlQjGw4AMES4sgcAAADAkImIg7Yv\nlvQtNW69fl1EPNTmtgCgqzHYAwAAAGBIRcRSSUvb3QcAHC34GBcAAAAAAEAX4coeAAAAACNOFlya\nhagOJPQ3q7UKTC0Nss2WmQW4ZsvLAmVLA5Cz4N5suqyXLNy5dDsyrfZhFtQ7adKkWm3y5Mm12pw5\nc4rWncm2Lwsc3r59e62W9Vx63EuPcWl/krR8+fJaLTsuEydOLOox274s7DjrJzvO8+bNK+pl9+7d\ntVq2v7JaFj6dbVur0OWBhCcPJFB5ICHQGa7sAQAAAAAA6CIM9gAAAAAAAHQRBnsAAAAAAAC6CIM9\nAAAAAAAAXYSAZgAAAAAdLQsuLQ1HLQ1oLl1HqxDV0oDa0vVkobrZvNl6s+meeOKJWi0Lt80CebNA\n5Ky/vXv3FvXSyvjx42u1PXv21Grjxo2r1bZu3Vq8nt5KA5BPOOGEI15HaUBwVutPuPDKlStrtV27\ndtVqpfs6Ox+yfTNhwoSi6bJw56y2b9++Wi3b5qy/ge7D6dOn12rZczQLli59Lmd9Z7LnaCmu7AEA\nAAAAAOgifQ722F5o+/u2l9t+yPZ7h6MxAED72T7X9iO2V9i+LHl8nO0bqsfvtr1o+LsEAAAA0Kzk\nyp6Dkt4XES+U9HJJ77F96tC2BQBoN9ujJH1a0nmSTpX0tuTn/0WStkTESZKulPR3w9slAAAAgN76\nHOyJiPURcU/19Q5JyyXNH+rGAABtd6akFRGxKiL2S/qKpAt6TXOBpOurr2+S9GpnH4oGAAAAMGz6\nlfZTXZ5/hqS7k8eWSFoiSccff/wgtAYAaLP5ktY0fb9W0staTRMRB21vkzRT0ubmiZp/RwAA0F9Z\n+GsWhJoFwmYBp1kwaxb6Wxqg22rdWTh0Vit9nySbbrBrpbJ5SwOfW603C+rNQoOzY59Nl8l6LD2X\nHn300VotC9/NzqVseVlt7NixtdqoUaNqtd27d9dqrdadbfOWLVuKpsu2LwvszsK5s+OZbV/WczZd\n1l82b6Y0hFuSduzYUatt27atVsv2TVbL9kM2XSbru1RxQLPtYyR9VdKlEbG99+MRcXVELI6IxbNn\nzz7ihgAAHSN7Jdb7FW7JNM/6HTEonQEAAABoqWiwx/YYNQZ6vhgRXxvalgAAHWKtpIVN3y+QtK7V\nNLZHS5oq6alh6Q4AAABAquRuXJZ0raTlEfGJoW8JANAhfizpZNvPtT1W0oWSbu01za2S3lF9/RZJ\n34tW17cDAAAAGBYlV/acJentks62fW/17/wh7gsA0GYRcVDSxZK+pUY4/40R8ZDtj9l+QzXZtZJm\n2l4h6c8l1W7PDgAAAGB49RnQHBF3Ks9kAAB0uYhYKmlpr9qHm77eK+l3h7svAMDIYnu1pB2SnpZ0\nsL8Zbln4bhZ0nAXtZgHB2UWopQG6rQx2KHK2fVlQb2lt3759RevNgmOzeUv3Yba8bNukvO9Mabht\nqWxbsnOpdL3Z8kqP+0C3LduHWT/Tpk2r1bIet27dWqtl+yZbXva8zc6lLMA4q2XzZrVsH5SGcEvS\nokWLarVZs2bValk4dHb8svUMJJS9VL/uxgUAAAAAR+i3ImJz35MBAAaq+G5cAAAAAAAA6HwM9gAA\nAAAYaiHp27aX2V7S7mYAoNvxMS4AAAAAQ+2siFhn+1hJ37H9cETccejBagBoiSRNnTq1XT0CQNdg\nsAcAAADAkIqIddX/G23fLOlMSXc0PX61pKslaf78+bU02SyktDRwdfTo+p88rQKCS9aRhd1K0kte\n8pJaLeu7NFg6W08WPFsa6PuLX/yiVsv2TbaObLqs52zb+hM4XBpsnK2ndL9mtdJQ44Hs/1LZ9pb2\nN9B+snVn68nWkQUq7969u1abMmVKrVZ6/pf2UnoutPo5sH79+lpty5YttdrkyZNrtexnULZvsn4y\nY8aMKZouw8e4AAAAAAwZ25NsTz70taTXSnqwvV0BQHfjyh4AAAAAQ2mOpJurqwZGS/pSRHyzvS0B\nQHdjsAcAAADAkImIVZJe3O4+AOBowse4AAAAAAAAughX9gAAAADoaKWBq1mQbWnAbxb+WjqdJC1b\ntqyonyzsuFQ2765du4qmKw1jLg0mzvb/3r17i6Zr5ZhjjqnVsmOQ9VMaul16jpTum1bnQ4nS5fVn\nvVkYcGlw9rZt22q1ffv21WoTJ06s1bIw5my9WeDwwYMHa7Vs/2e1cePGFa03Ow+z495qPQN5/pQG\nS5f2UoorewAAAAAAALoIgz0AgJZsn2v7EdsrbF+WPP5O25ts31v9e3c7+gQAAADwK3yMCwCQsj1K\n0qclvUbSWkk/tn1rRPy016Q3RMTFw94gAAAAgBRX9gAAWjlT0oqIWBUR+yV9RdIFbe4JAAAAQB+G\n5MqeZcuWtQw7QncZSCDZcOFcBI7YfElrmr5fK+llyXRvtv1KST+T9GcRsSaZBoPsqquuancLXeHG\nG29sdwtF3vrWt7a7hT794Ac/aHcL6GJZIGwWuJqFtWavV9euXVurTZo0qWh5rYJVs/rChQtrtUcf\nfbRWGz9+fK2WBe1mAchZ39m82Tp27txZq2Wvnbds2VKrlQZkZ+G7WRCzlIdNZx577LFaLQv+zYJ6\ns31TGt5bGu6chRpPmzatqFa6r1sFfWf9TJgwoVbLzqUZM2bUalu3bq3VsufFKaecUqvdd999tdrp\np59eq23cuLFWmz9/fq2W7Yd58+bVatlzrD9h6Rs2bKjVsudPZsqUKbVadl6XBsIPJNCdK3sAAK1k\nI6W9fzP9q6RFEfHrkv5d0vXpguwltnts9wxyjwAAAAB6YbAHANDKWknNb0kukLSueYKIeDIiDr19\n9VlJL80WFBFXR8TiiFg8JJ0CAAAA+CUGewAArfxY0sm2n2t7rKQLJd3aPIHtuU3fvkHS8mHsDwAA\nAECCu3EBAFIRcdD2xZK+JWmUpOsi4iHbH5PUExG3SrrE9hskHZT0lKR3tq1hAAAAAJIY7AEAHEZE\nLJW0tFftw01ff0DSB4a7LwAAAACtMdgDAAAAoKNldwnK7maT3SUouxtUdues7E442Tpa3Y1227Zt\ntVp2Z6SxY8fWatmdgrI7kJXK7r6U7Yfs7kZZLes5W162D7Naq7tuZX2X3ikrmy5bd6nsOGd3Rsp6\nye4Mlp2bpXeIyrYjm1fKj0vpeVg6XbZ9mWze7M6N2R3lpk+fXqtt3ry5Vsuey3PmzKnVsjvwnXDC\nCbWalN9568CBA7Va9hzNjkt2LpUep4H8HCCzBwAAAAAAoIsw2AMAAAAAANBFGOwBAAAAAADoIgz2\nAAAAAAAAdBECmgEAAAAMmO3rJL1e0saIOK2qzZB0g6RFklZLemtEbOnvsh977LFaLQvkzUJwS0OI\ns2DVLPw1C1aVpGOOOSatl8yfBfDu37+/qJaF4O7bt69WG0jwb7beLHQ2my5bR9aLlG9LNn92/LJl\nZtuSzZvJ1psFE2fLK903WSB1to5sO7Jj3Kqf0nM7CwjOpivdh6XPnylTptRq2fZloc1r1qyp1Z54\n4olabf78+bXazp07azWpfH/t2bOnVsvOw71796brGcx5M1zZAwAAAGAwfE7Sub1ql0n6bkScLOm7\n1fcAgCHGYA8AAACAAYuIOyQ91at8gaTrq6+vl/TGYW0KAI5SDPYAAAAAGCpzImK9JFX/H5tNZHuJ\n7R7bPbt27RrWBgGgGzHYAwAAAKCtIuLqiFgcEYsnTZrU7nYAYMQjoBkAAADAUNlge25ErLc9V9LG\nwVpwFqCbBcxmYaulsqDdVssrDbLNpsvWU7qOgdSy0OBsH44fP75Wy4Jjs4DZAwcOFE0n5eG92fyZ\nbB9m+z9bR3ZMs/VmV51l52G2jmwfZtOVriM7nlK+b7Pg62x/Zcc+C4xudfxK1puFIm/evLlWK33u\nbN++vVbL9uFTT/X+hGlrCxYsKOonUxooXhpmPnHixKL1pr0c8ZwAAAAAcHi3SnpH9fU7JN3Sxl4A\n4KjR52CP7fG2f2T7PtsP2f5vw9EYAKD9bF9ne6PtB1s8btv/ZHuF7fttv2S4ewQAdAbbX5b0H5Je\nYHut7YskXSHpNbZ/Luk11fcAgCFWcv3VPklnR8RO22Mk3Wn7GxFx1xD3BgBov89J+pSkz7d4/DxJ\nJ1f/XibpM9X/AICjTES8rcVDrx7WRgAAfV/ZEw2HPlg3pvpX9qFSAMCI1uI2us0ukPT56nfFXZKm\nVZkMAAAAANqkKFnJ9ihJyySdJOnTEXF3Ms0SSUsGtz0AQIebL2lN0/drq9r65on4HQEAGIgsOLY0\n6Lg0uDcLnS0N35XyINts/qyWzZv1mE2X1Upl6yjd5nHjxtVqWehsNl2rsNvjjz8+rfeWHfssZLn0\nfCgNQH788cdrtWxbSvdr6Xb0J6C5NIg7C6DO9k1pkHAmm3ffvn212pQpU4rmXbhwYa2W7evZs2fX\nallo83HHHVerSXnoc6v93Vv2cyT7+ZWFV5f+DCpVNGdEPB0Rp0taIOlM26cl0/zydolH3A0AYKSp\n//ZKrv7kdwQAAAAwfPo1TBQRWyXdLuncIekGADDSrJXU/DbLAknr2tQLAAAAAJXdjWu27WnV1xMk\nnSPp4aFuDAAwItwq6Q+qu3K9XNK2iFjf10wAAAAAhk5JZs9cSddXuT3PkXRjRNw2tG0BADpBdRvd\nV0maZXutpI+oEdSviLhK0lJJ50taIWm3pHe1p1MAAAAAh/Q52BMR90s6Yxh6AQB0mMPcRvfQ4yHp\nPcPUDgDgKDVjxoxaLQuTzQJOs2DVLNQ1C1EtDYaWpL179xb1UxrgmgWzDsd0rcKTeysNbc6Wl4XT\nStKqVauK5s+OfWmwcRZMXDrd5MmTa7Xx48fXaqVhvqUhvaXh31J+HpYaSMB56fJmzpxZq2XnUhbQ\nvHr16lot296sv507d9ZqjzzySK0mSfPmzavVsvOr1XlcMm+2zdnzcSCOPNoZAAAAAAAAHYfBHgAA\nAAAAgC5Jc08tAAAgAElEQVTCYA8AAAAAAEAXYbAHAAAAAACgi5TcjQsAAAAA2mbHjh21WhbCmoXl\nZkGo2bxZMG42b6vw3SystXT+LAw4q2XLy8JfS0Ops+my7ch6KV1eFiScBfdK+fErDQMuVRpKnU03\nceLEWq001DjbD9mxy/ZBf/Zhq+Dm3krDgEvDvktlId4TJkyo1fbt21erTZs2rVbLjsmKFSuK5p0z\nZ07aY2lgd+m5mS0v2w/Z+TCQ0Gau7AEAAAAAAOgiDPYAAAAAAAB0EQZ7AAAAAAAAugiDPQAAAAAA\nAF2EgGYAAAAAA2b7Okmvl7QxIk6rah+V9EeSNlWTXR4RS/u77NKg4ywINQtRLQ1Wzdab1SRpypQp\nRcvMAnizYNYxY8bUaqUhv5kTTzyxVtu/f3+tlgU0Z9NlvWTHJAuibWXlypVFyxxIyPJAptuyZUvR\ndKXryLZt69attVq2r7NjIkmTJk1K6yWykPLsfMjOzUw27+rVq2u1rOeTTz65Vvv5z39eq02dOrVW\nmzx5cq2WBT6vWbOmVpOk4447rlbbu3dvrZYFS5cGl5eeI9k+LMWVPQAAAAAGw+cknZvUr4yI06t/\n/R7oAQD0H4M9AICWbF9ne6PtB1s8/irb22zfW/378HD3CADoDBFxh6Sn2t0HAIDBHgDA4X1O+bu0\nzX7Y9I7tx4ahJwDAyHKx7furNxCmZxPYXmK7x3bPrl27hrs/AOg6DPYAAFriXVoAwAB9RtLzJJ0u\nab2kf8gmioirI2JxRCweSOYIAKCBgGYAwED9hu37JK2T9BcR8dDhJn7pS1+qnp6e4ensCGUBeehO\nb33rW9vdQte4+eab290COlBEbDj0te3PSrrtSJZTGhybBaHOmjWrVtu8eXOtNnv27Frtqafq73e0\nChyOiFpt06ZNtVoWHrt79+5aLQtezgbCsqDehQsX1mqrVq0qmjf7HZgF0WaBtZnnPve5tdqKFSvS\nabPty/bDzJkza7UNGzbUalkIbmmgdXbeZFedZcG/2TmSnUuloc2ZU045Ja3fddddtVp2nCdOnFir\nZfsrO86nnXZarfaDH/ygVsueU/PmzavVsvNr48aNtdqCBQtqteyYZMvLjkn2vGu1nnvuuadWy/br\n9On1ixf37NlTq5Xu/1Y9luDKHgDAQNwj6YSIeLGk/ynp69lEzZfnZy98AQDdyfbcpm/fJCnNgAMA\nDC4GewAARywitkfEzurrpZLG2K69FdZ8eX72Lg8AYOSz/WVJ/yHpBbbX2r5I0sdtP2D7fkm/JenP\n2tokABwl+BgXAOCI2T5O0oaICNtnqvEmwpNtbgsA0AYR8bakfO2wNwIAYLAHANBa9S7tqyTNsr1W\n0kckjZGkiLhK0lsk/Yntg5L2SLowstACAAAAAMOGwR4AQEst3qVtfvxTkj41TO0AAI5SWXBpqSyQ\nN1vegQMHarUsLLdVwG+2zNLawYMHi6bLZD1mwb/PeU49wSMLvs6mK11eVstk620l2w/Z+0qD/V5T\nti2lxy4LD8/mLQ1o7s+NI84///xabevWrbVaFiw9ZsyYWm3GjBm1Wmmw9/Of//xabcuWLbVatq+z\n/ZqFLGf7q/RcaDXdtm3barVsP6xfv75Wy/ZhJsuwzIKc58+fX7S8DJk9AAAAAAAAXYTBHgAAAAAA\ngC7CYA8AAAAAAEAXYbAHAAAAAACgixDQDAAAAKCjjR8/vlbLAmGzUNdMFvSa1bL1tgpOzgKeswDY\nbLqs7ywIOgujzWpZ+G42XbbebL+W9lIaSJ3tg1brGUgYc+m82TZn05UGBJeG9GbryMKdM63CsG+5\n5ZZaberUqbVati3Zfpg4cWKttmHDhlpt9uzZtdqdd95Zq02YMKGoloUVT5o0qVbbtWtXrZbtm+y5\nnM0rSS94wQtqtQcffLBWy47Vk08+Wasdf/zxtdrGjRtrtazvgQSPc2UPAAAAAABAF2GwBwAAAAAA\noIsw2AMAAAAAANBFGOwBAAAAAADoIgQ0AwAAAOhoWZDqQEKDs/DXvXv3Fq23VTBuq9Dh3rJg3Gzd\n2fZl25KFLGe9ZOG7o0eX/TmY9ZLVMtn+Kg0wbqX0OA92QHMWfJ3t/2yb9+/ff8T99Se4N9uWY489\ntlbLzsN77723Vps8eXKttnPnzlotO6bbt2+v1Y477rhaLTsPs/1VGpBdGtTe6jm7bt26Wi07Bllg\n9ObNm4uWl+3X3bt3F/dYgit7AAAAAAAAugiDPQAAAAAAAF2keLDH9ijbP7F921A2BADoDLYX2v6+\n7eW2H7L93mQa2/4n2yts32/7Je3oFQAAAMCv9OfKnvdKWj5UjQAAOs5BSe+LiBdKermk99g+tdc0\n50k6ufq3RNJnhrdFAAAAAL0VJXLZXiDpdZL+RtKfD2lHAICOEBHrJa2vvt5he7mk+ZJ+2jTZBZI+\nH42UwLtsT7M9t5oXAHCUsL1Q0uclHSfpGUlXR8Qnbc+QdIOkRZJWS3prRGzp7/IHGujbWxYunAXb\nZuvNAmElaebMmbVaFqI7YcKEWi0LqM36yWpZGO306dNrtSeffLJo3kw2XbYfsv4yrUJns/2dBS9n\nYblZLZs3O/alochTpkyp1bJQ3Ww7xo4dW6tlSgOkWwVkP//5zy9az8qVK2u1RYsW1WrZ9mXb8tRT\nT9Vq48ePr9Wycz3blmz/lwaFZ8c9OyZZCLQkzZ07t1bLti8LY85CpLOfDRs2bCiaN9v/pUqv7PlH\nSX+pxg/ulO0ltnts9xxxNwCAjmR7kaQzJN3d66H5ktY0fb+2qvWe/5e/IzZt2jRUbQIA2qfV1aCX\nSfpuRJws6bvV9wCAIdbnYI/t10vaGBHLDjddRFwdEYsjYvGgdQcAaDvbx0j6qqRLI6L3PTSzt/Bq\nb0k1/46YPXv2ULQJAGijiFgfEfdUX+9QI/5hvhpXgF5fTXa9pDe2p0MAOLqUXNlzlqQ32F4t6SuS\nzrb9hSHtCgDQEWyPUWOg54sR8bVkkrWSFjZ9v0DSuuHoDQDQmXpdDTrn0Ed7q/+PbTHPL68A3bVr\n13C1CgBdq8/Bnoj4QEQsiIhFki6U9L2I+P0h7wwA0FZufED8WknLI+ITLSa7VdIfVHflermkbeT1\nAMDRq4+rQVtqvgJ00qRJQ9cgABwligKaAQBHpbMkvV3SA7bvrWqXSzpekiLiKklLJZ0vaYWk3ZLe\n1YY+AQAdoMXVoBsOBffbnitp45EsOxsAKg1PzkJPJ0+eXKtNnTr1SFr7pe3by8a2soDaHTt2FE2X\nycKTs+WVBj6X1kr1Z3lZKG+2H7JjWhrem62jNAA5C/QtDRLOjlNpqHTWXzadJK1du7ZWmzFjRq2W\nfaw+6zF7XqxfX39fLwuG/tnPflarlQZQZ/smm7fVfhiILF8y+3mTBXaXBlBn+zW7qrFVmHmJfg32\nRMTtkm4/4rUBAEaMiLhTeSZP8zQh6T3D0xEAoFMd5mrQWyW9Q9IV1f+3tKE9ADjqcGUPAAAAgIFq\ndTXoFZJutH2RpMck/W6b+gOAowqDPQAAAAAGpI+rQV89nL0AAMruxgUAAAAAAIARgit7AAAAAHS0\n0sDhLKA5C0fdtm1brTZ+/PhabevWraUtFoft7tu3r3iZJbJtzmpZuG02Xel+LQ0SLp1Xyo9B6baM\nHTs2XWbJvKW13bt312pZqG4WIJ2FHw8kcHju3LlpPQsK37t3b6126qmn1mpZuPOECRNqtWnTptVq\nmzdvrtVKQ5b7c46ULC87D/uzr7Pte+yxx4r6Kd3mbB2l05Xiyh4AAAAAAIAuwmAPAAAAAABAF2Gw\nBwAAAAAAoIsw2AMAAAAAANBFCGgGAAAA0NFKg4RLQ11LA2FL1yuVB7OOGTOmVstCm0uXlwVQZ7Xp\n06fXavv37z/i5ZXumyzgd+bMmbWalO+HAwcO1GqTJ0+u1bJtyfZXpvRcKg1yLg3rbnUuDUR2nI8/\n/vha7ac//WmtdtJJJ9VqO3furNXmzZtXq61Zs6ZWy47T6NH1IYhsH5ZON5Bg7lbzZuvOzq+slm1z\nZuPGjbVadq5nz71SXNkDAAAAAADQRRjsAQAAAAAA6CIM9gAAUrYX2v6+7eW2H7L93mSaV9neZvve\n6t+H29ErAAAAgF8hswcA0MpBSe+LiHtsT5a0zPZ3IqL3h7x/GBGvb0N/AAAAABIM9gAAUhGxXtL6\n6usdtpdLmi+pnugHAMAQyoJssxDVLOD0ySefrNWyAN0nnniiaLpWoa5Zj9m0W7ZsqdWOPfbYWm3D\nhg21WrbNs2bNqtUef/zxWm3KlClF/WXbsXfv3lots2fPnlotC/jNwp2lPPA2C21+0YteVKs99NBD\ntdr48eNrtWxbZs+eXatlxynbN9k6suM0bty4Wi2TBfJm53V2PCXplltuqdWWLVtWq73sZS+r1c49\n99xa7UMf+lCtduedd9Zq2fma7ZsvfOELtdrBgwdrtezczM6brHbiiSfWaqtWrSrqT5J+9KMf1WpZ\nwPO0adNqtazvrJYd04kTJ9ZqTz31VNpjCT7GBQDok+1Fks6QdHfy8G/Yvs/2N2z/2rA2BgAAAKBm\nSK7seelLX6qenp6hWDTQb6W3XASGw+LFi9vdQr/ZPkbSVyVdGhHbez18j6QTImKn7fMlfV3Sycky\nlkha0vT9EHY8cPPnz293C326//77291Cn1rdWhfd6eabb253CwAAoMKVPQCAlmyPUWOg54sR8bXe\nj0fE9ojYWX29VNIY27XrySPi6ohYHBEjb7QLAAAAGGEY7AEApNy4/OZaScsj4hMtpjmumk62z1Tj\n90o9HAEAAADAsCGgGQDQylmS3i7pAdv3VrXLJR0vSRFxlaS3SPoT2wcl7ZF0YfDZSQA46theKOnz\nko6T9IykqyPik7Y/KumPJG2qJr28uhIUADCEGOwBAKQi4k5Jhw3XiYhPSfrU8HQEAOhgByW9LyLu\nsT1Z0jLb36keuzIi/n6wV5jdKav0/YbS6fqTMVe6zOwOQNm82R2dstozzzxTq40ZM6ZoHdldkLJ5\nS/dDNl3WX6vlZXfeyu5QlN1Ra/r06bVa6d2bsjsezZgxo1bL7qCU7ddsm0trA801fO1rX1ur7dq1\nq1a75557arWLLrqoVvvgBz9Yq11wwQW12itf+cpaLbsT26WXXlqrlZ6bpfswO8a7d+8umlfK7842\nYcKEdNresnN4zpw5tVp2d7Zsf2V32yvFYA8AAACAAYmI9ZLWV1/vsL1cUuen3QNAlyKzBwAAAMCg\nsb1I0hmS7q5KF9u+3/Z1tuuXXwAABh2DPQAAAAAGhe1j1LiL46URsV3SZyQ9T9Lpalz58w8t5lti\nu8d2T/aREwBA/zDYAwAAAGDAbI9RY6DnixHxNUmKiA0R8XREPCPps5LOzOaNiKsjYnFELJ40adLw\nNQ0AXYrMHgAAAAAD4kaq7LWSlkfEJ5rqc6s8H0l6k6QHj2T5pUG7mdJQ19J5WwUxZ9NmIdJZgGs2\nwJWFAWeyQN9s3mnTptVqWVh0FgKd7etRo0YV9VcaKi3lAbX79++v1bLty0KWx44dW9JiGsacXWGW\nhVcPRHbssn2dnXOtzsMsKPmFL3xhrfaNb3yjVsv2/0knnVSrbd26tVZbt25drbZmzZpa7brrrqvV\nMtm+yc6l0n2TzdsqDHvbtm212pYtW2q17FhNnjy5VtuzZ0+tlp1f2fNiw4YNaY8lGOwBAAAAMFBn\nSXq7pAds31vVLpf0NtunSwpJqyX9cXvaA4CjC4M9AAAAAAYkIu6UlL1NvnS4ewEAkNkDAAAAAADQ\nVRjsAQAAAAAA6CJ8jAsAAABARysNWc4CU7OQ5P4EL5esQ5ImTJhQq5UGBGdhx1kYcBYym8nm3b17\nd61WGlSdbXMWNJ2toz8BzbNmzarVsjDgLLR59uzZtVp2TEuDf7NA6+3btxetIzvnDh48WKtlsnOh\nNIxckj70oQ/Vah//+Mdrtcsvv7xWy8KTL7nkklrts5/9bK328MMP12onn3xyrfb+97+/Vis9Ttl0\nAwlbbxXQnNWzY5o956dOnVqrZWHMWaB46c+vUlzZAwAAAAAA0EUY7AEAAAAAAOgiRdcB2l4taYek\npyUdjIjFQ9kUAKD9bI+XdIekcWr8vrgpIj7Sa5pxkj4v6aWSnpT0exGxephbBQAAANCkP5k9vxUR\nm4esEwBAp9kn6eyI2Gl7jKQ7bX8jIu5qmuYiSVsi4iTbF0r6O0m/145mAQAAADQQ0AwASEUjBW9n\n9e2Y6l/vZLwLJH20+vomSZ+y7ShNuQQAoEAWoJsFl2a1LPA2+zWV1bJ5s5qUhwZnobB79+6t1Y45\n5phaLQtALu07m3f8+PG1Wqlsv2brzY5Tq/2V2bRpU62WhdbOnDmzVvvJT35Sq2VBuxMnTizup7cs\n+Lo0oLk0aLc0ULyVv/qrv6rVlixZUqtl+2HlypW12tKlS2u1z3zmM7XaTTfdVKtlgdZXXnllrVb6\n3BtIgHF/go5POumkWi07BlkA+4EDB2q1KVOm1Gpz586t1bIed+7cWauVKt3ikPRt28ts188USbaX\n2O6x3ZM9SQEAI4/tUbbvlbRR0nci4u5ek8yXtEaSIuKgpG2Saq/Amn9HDHXPAAAAwNGudLDnrIh4\niaTzJL3H9it7TxARV0fE4ohYnN32DgAw8kTE0xFxuqQFks60fVqvSbJ7Vtbenmn+HTEUfQIAAAD4\nlaLBnohYV/2/UdLNks4cyqYAAJ0lIrZKul3Sub0eWitpoSTZHi1pqqSnhrU5AAAAAM/S52CP7Um2\nJx/6WtJrJT041I0BANrL9mzb06qvJ0g6R9LDvSa7VdI7qq/fIul75PUAAAAA7VUS0DxH0s1VuNVo\nSV+KiG8OaVcAgE4wV9L1tkep8ebAjRFxm+2PSeqJiFslXSvpn22vUOOKngvb1y4AoFuVBtRmgbyl\nocaZ/sw7bty4Wi0L9M1CWLNg42x5WWhtNl1Wy/rO+st62bVrV62WhdOWhja3CsvNgpezY5+FYU+e\nPLlWG0g4d1bL9msm2zelSo9JK1nI8jnnnFOrZUHCDzzwQK320Y9+tFa79NJLi9b7l3/5l7Xacccd\nV6tl+zo77tnzOwttLv050Mqjjz5aq+3YsaNWy/ZhFraeBTRny8vO4YEENPd51kTEKkkvPuI1AABG\npIi4X9IZSf3DTV/vlfS7w9kXAAAAgMMrv/8YAAAAAAAAOh6DPQAAAAAAAF2EwR4AAAAAAIAuUp70\nBAAAAAAt2B4v6Q5J49T4O+OmiPiI7edK+oqkGZLukfT2iKgn7B5GaYBrVsvCgEsDefsT9JqF92br\nPnjwYK2WBQlnQb1ZLZs3C/TN5s0MJMC4tNYqcDsL2926dWutdtJJJ6Xz95bt6927d9dqEyZMqNVK\nz69s+7IA6Uw2b7YPsu1o5cYbb6zVnnzyyVpt6dKltVoWLpydw9l011xzTa32pje9qVb7wz/8w1ot\nkz33SkObM/05D7PnSjZ/FiiezTt+/PhaLQtozuYtPZcyXNkDAAAAYDDsk3R2RLxY0umSzrX9ckl/\nJ+nKiDhZ0hZJF7WxRwA4KjDYAwAAAGDAouHQfYLHVP9C0tmSbqrq10t6YxvaA4CjCoM9AAAAAAaF\n7VG275W0UdJ3JK2UtDUiDn0OZa2k+cl8S2z32O7ZtWvX8DUMAF2KwR4AAAAAgyIino6I0yUtkHSm\npBdmkyXzXR0RiyNi8aRJk4a6TQDoegQ0AwAAABhUEbHV9u2SXi5pmu3R1dU9CySt6+/yTjnllEHu\ncPA99thjRdNlobD79u2r1fbs2VOrHThwoFbLwl+zebMg5yx0NguaLg23zforDSGWygOxt23bVqtl\nQbZZsPHUqVNrtbFjx9Zq27dvL1pe1l+2r0vDnUsDxVt597vfXatdccUVtdqf/umf1mp33HFHrXbZ\nZZfVameffXatlgVDv+IVr6jVSvdh6blQul+zc65VQPPOnTvTem/ZeThv3rxaLbtaMTtft2zZUqtl\n52YpruwBAAAAMGC2Z9ueVn09QdI5kpZL+r6kt1STvUPSLe3pEACOHlzZAwAAAGAwzJV0ve1Raryp\nfGNE3Gb7p5K+YvuvJf1E0rXtbBIAjgYM9gAAUrbHS7pD0jg1fl/cFBEf6TXNOyX9D0mPV6VPRcQ1\nw9knAKAzRMT9ks5I6qvUyO8BAAwTBnsAAK3sk3R2ROy0PUbSnba/ERF39Zruhoi4uA39AQAAAEgw\n2AMASEUj3e5QQt2Y6l95QiAAAEdg3bp1mz/4wQ8+Wn07S9LmdvYzSLplO6Tu2ZZu2Q7pCLZl5cqV\nRdN95CMfKaoNkqP6mPTDCSUTMdgDAGipyl1YJukkSZ+OiLuTyd5s+5WSfibpzyJizXD2CADoLhEx\n+9DXtnsiYnE7+xkM3bIdUvdsS7dsh9Q929It2yF1xrYMyWDPsmXL0lulofv05zaA7cK5CBy5iHha\n0unV3VVutn1aRDzYNMm/SvpyROyz/X9Jul5S7X6ctpdIWlJ9u1PSI4Pc6qC+e/L444/3PVH/DWqP\nM2fOHKxFNRsJ76jR4+Cgx8ExFD0WvWMLAMDhcGUPAKBPEbHV9u2SzpX0YFP9yabJPivp71rMf7Wk\nq4eqv05496Qv9Dg46HFw0OPgGAk9AgCOTs9pdwMAgM5ke3Z1RY9sT5B0jqSHe00zt+nbN0haPnwd\nAgCOAkP2RsEw65btkLpnW7plO6Tu2ZZu2Q6pA7aFK3sAAK3MlXR9ldvzHEk3RsRttj8mqScibpV0\nie03SDoo6SlJ72xbtwCArlNdGTridct2SN2zLd2yHVL3bEu3bIfUGdvCYA8AIBUR90s6I6l/uOnr\nD0j6wHD21ULbf6EWoMfBQY+Dgx4Hx0joEQBwFPJQBOza7vzUXgwKApqB/osITkoAAAAAQ4bMHgAA\nAAAdxfa5th+xvcL2Ze3upz9sX2d7o+0Hm2ozbH/H9s+r/6e3s8cSthfa/r7t5bYfsv3eqj4St2W8\n7R/Zvq/alv9W1Z9r++5qW26wPbbdvZawPcr2T2zfVn0/Urdjte0HbN9ru6eqjcTza5rtm2w/XD1f\nfqMTtoPBHgDAiDUS/hjIXvR3mlYv6DtJqxfqnab3C/BOlL247jTZC+d299TM9guq/Xfo33bbl7a7\nr25RZcV9WtJ5kk6V9Dbbp7a3q375nBp3r2x2maTvRsTJkr5bfd/pDkp6X0S8UNLLJb2nOg4jcVv2\nSTo7Il4s6XRJ59p+uRp3Eb2y2pYtki5qY4/98V49+6YYI3U7JOm3IuL0pjsbjsTz65OSvhkRp0h6\nsRrHpu3bwWAPAGBEGkF/DHxO9Rf9nabVC/pO0uqFeqfp/QK8U/V+cd1pshfOHSMiHqn23+mSXipp\nt6Sb29xWNzlT0oqIWBUR+yV9RdIFbe6pWETcocZNC5pdIOn66uvrJb1xWJs6AhGxPiLuqb7eocbz\ncL5G5rZEROysvh1T/QtJZ0u6qaqPiG2xvUDS6yRdU31vjcDtOIwRdX7ZniLplZKulaSI2B8RW9UB\n28FgDwBgpBoRfwy0eNHfUQ7zgr5jHOaFesfo/QIcR+YwL5w71aslrYyIR9vdSBeZL2lN0/dr1WE/\nk47AnIhYLzV+5ko6ts399IvtRWrctOFujdBtqa68vFfSRknfkbRS0taIOFhNMlLOs3+U9JeSnqm+\nn6mRuR1S4/f4t20vs72kqo208+tESZsk/a/qyt5rbE9SB2wHgz0AgJGqG/8YaLteL+g7Su8X6hHR\naT32fgHeqbIX152k1QvnTnWhpC+3u4kuk93IoKMGd48mto+R9FVJl0bE9nb3c6Qi4unqarwFarxh\n9MJssuHtqn9sv17SxohY1lxOJu3o7WhyVkS8RI2rtN9j+5XtbugIjJb0EkmfiYgzJO1Sh3z0jMEe\nAMBINZJf3HSkTn9B3/uFuu3T2t3TIS1egHeqTn9x3bEvnHurQlDfIOlf2t1Ll1kraWHT9wskrWtT\nL4Nlg+25klT9v7HN/RSxPUaN3wtfjIivVeURuS2HVFcK3q7Gx5an2R5dPTQSzrOzJL3B9mo1rmg+\nW403GkbadkiSImJd9f9GNT4Ke6ZG3vm1VtLapjegblLjd1jbt4PBHgDASNWNfwy0TYsX9B2p6YV6\nJ2Uh1V6A2/5Ce1vKtXhx3UlavXDuROdJuiciNrS7kS7zY0knV3cYGqvG1VO3trmngbpV0juqr98h\n6ZY29lKkyoK5VtLyiPhE00MjcVtm255WfT1B0jlqfGT5+5LeUk3W8dsSER+IiAURsUiN58X3IuL/\n1AjbDkmyPcn25ENfS3qtpAc1ws6viHhC0hrbL6hKr5b0U3XAdjDYAwAYqbrxj4G2OMwL+o7R4oX6\nw+3t6ldavAD//Ta3VXOYF9cd4zAvnDvR28RHuAZdlT1ysaRvqfEH+Y0R8VB7uypn+8uS/kPSC2yv\ntX2RpCskvcb2zyW9pvq+050l6e1qDF4fuvPc+RqZ2zJX0vdt36/G64fvRMRtkt4v6c9tr1Aj++ba\nNvY4ECNxO+ZIutP2fZJ+JOnfIuKbGpnn159K+mJ1fp0u6W/VAdvhiMG/4t02l9EfJYbi/Blsjb9h\ngM4REZyUg6R60fmPkkZJui4i/qbNLdVUL/pfJWmWpA2SPhIRHfUizPYrJP1Q0gP6Vd7M5RGxtH1d\nPZvtX1fjbhaj1Hiz6saI+Fh7u8rZfpWkv4iI17e7l95sn6hf3TVqtKQvdejz5nQ1gq7HSlol6V0R\nsaW9XT2b7Ylq5IadGBHb2t0PAADNGOzBgDDYA/Qfgz0AAAAAhhIf4wIAAAAAAOgiRYM9tqfZvsn2\nw7aX2/6NoW4MAAAAAAAA/Te670kkSZ+U9M2IeEsVgjlxCHsCAAAAAADAEeozs8f2FEn3qRE+VxTQ\nQmbP0YPMHqD/yOwBAAAAMJRKPsZ1oqRNkv6X7Z/Yvqa6Veez2F5iu8d2z6B3CQAAAAAAgCIlV/Ys\nloo31FYAAAdkSURBVHSXpLMi4m7bn5S0PSI+dJh5Ov9yDwwKruwB+u//b+/uXi2ryziAf59mlNIS\nL4wQRzIh5qIuVEQJQaKpUBLrUqEuvLGLCocuorqJ/oHoLojRMDKlNCEieoGK6iJTJ8N8KVSUOZpO\n0ovZjVRPF7OCOWfOnLO37dNae83nA5uz1z6LH182i3PxPev5LXf2AAAAe2mRO3s2kmx09wPD8b1J\nrti7SAAAAAC8XruWPd39YpJjVXVw+OhQksf3NBUAAAAAr8uuY1xJUlWXJTmS5OwkzyS5pbv/ssP5\n05/tYSWMccHyjHEBAAB7aaGyZ+lFlT1nDGUPLE/ZAwAA7KVF9uwBAAAAYE0oewAAAABmRNkDAAAA\nMCPKHgAAAIAZUfYAAAAAzIiyBwAAAGBGlD0AAAAAM6LsAQAAAJgRZQ8AAADAjCh7AAAAAGZE2QMA\nAAAwI8oeAAAAgBlR9gAAAADMiLIHAAAAYEaUPQAAAAAzouwBAAAAmBFlDwAAAMCMKHsAAAAAZkTZ\nAwAAADAjyh4AAACAGVH2AAAAAMyIsgcAAABgRpQ9AAAAADOi7AEAAACYEWUPAAAAwIwoewAAAABm\nRNkDAAAAMCPKHgAAAIAZUfYAAAAAzIiyBwAAAGBGlD0AAAAAM6LsAQAAAJiRXcueqjpYVY+c9Hql\nqg7/P8IBAAAAsJzq7sVPrtqX5PkkV3f3czuct/iirLVlrp+xVNXYEWCT7nZRAgAAe2bZMa5DSZ7e\nqegBAAAAYDz7lzz/piR3b/eLqro1ya3/cyIAAAAAXreFx7iq6uwkLyR5V3e/tMu505/tYSWMccHy\njHEBAAB7aZkxruuTHN2t6AEAAABgPMuUPTfnNCNcAAAAAEzDQmNcVXVOkmNJLu3uvy1w/vRne1gJ\nY1ywPGNcAADAXlrq0esLL6rsOWMoe2B5yh4AAGAvLfvodQAAAAAmTNkDAAAAMCPKHgAAAIAZUfYA\nAAAAzIiyBwAAAGBGlD0AAAAAM6LsAQAAAJgRZQ8AAADAjCh7AAAAAGZE2QMAAAAwI8oeAAAAgBlR\n9gAAAADMiLIHAAAAYEaUPQAAAAAzouwBAAAAmJH9e7Tuy0meW+F6FwxrTtkZmbGqVrlccoZ+j3tA\nxtXYi4xvX/F6AAAAm1R3j51hV1X1UHdfOXaOnci4GjKuhoyrsQ4ZAQAAtjLGBQAAADAjyh4AAACA\nGVmXsuerYwdYgIyrIeNqyLga65ARAABgk7XYswcAAACAxazLnT0AAAAALGDyZU9VXVdVv6+qp6rq\ns2Pn2aqq7qiq41X1u7GznE5VXVxVP62qJ6rqsaq6bexMW1XVG6vq11X12yHjF8fOdDpVta+qflNV\n3xs7y3aq6tmqerSqHqmqh8bOs52qOr+q7q2qJ4fr8j1jZzpZVR0cvr//vl6pqsNj5wIAAFjEpMe4\nqmpfkj8k+UCSjSQPJrm5ux8fNdhJquraJK8m+Xp3v3vsPNupqguTXNjdR6vqLUkeTvKRiX2PleTc\n7n61qs5K8sskt3X3r0aOdoqq+nSSK5Oc1903jJ1nq6p6NsmV3f3y2FlOp6ruTPKL7j5SVWcnOae7\n/zp2ru0Mf4eeT3J1dz83dh4AAIDdTP3OnquSPNXdz3T3a0nuSfLhkTNt0t0/T/LnsXPspLv/2N1H\nh/d/T/JEkovGTbVZn/DqcHjW8JpcE1lVB5J8KMmRsbOsq6o6L8m1SW5Pku5+bapFz+BQkqcVPQAA\nwLqYetlzUZJjJx1vZGIlxbqpqkuSXJ7kgXGTnGoYj3okyfEkP+7uyWVM8uUkn0ny77GD7KCT/Kiq\nHq6qW8cOs41Lk/wpydeGcbgjVXXu2KF2cFOSu8cOAQAAsKiplz21zWeTu9tjXVTVm5Pcl+Rwd78y\ndp6tuvtf3X1ZkgNJrqqqSY3FVdUNSY5398NjZ9nFNd19RZLrk3xiGDWckv1Jrkjyle6+PMk/kkxu\nP64kGUbMbkzy7bGzAAAALGrqZc9GkotPOj6Q5IWRsqy1YR+c+5Lc1d3fGTvPToaRnp8luW7kKFtd\nk+TGYU+ce5K8r6q+MW6kU3X3C8PP40nuz4lxyCnZSLJx0p1b9+ZE+TNF1yc52t0vjR0EAABgUVMv\nex5M8s6qesfwH/abknx35ExrZ9j8+PYkT3T3l8bOs52qemtVnT+8f1OS9yd5ctxUm3X357r7QHdf\nkhPX4k+6+6Mjx9qkqs4dNuHOMBr1wSSTelJcd7+Y5FhVHRw+OpRkMpuFb3FzjHABAABrZv/YAXbS\n3f+sqk8m+WGSfUnu6O7HRo61SVXdneS9SS6oqo0kX+ju28dNdYprknwsyaPDnjhJ8vnu/v6Imba6\nMMmdw5OP3pDkW909yUebT9zbktx/ot/L/iTf7O4fjBtpW59KctdQ4j6T5JaR85yiqs7JiScBfnzs\nLAAAAMuY9KPXAQAAAFjO1Me4AAAAAFiCsgcAAABgRpQ9AAAAADOi7AEAAACYEWUPAAAAwIwoewAA\nAABmRNkDAAAAMCPKHgAAAIAZ+Q9lLa9ZIf5DIgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7fe77e89bda0>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "matplotlib.rcParams['figure.figsize'] = (20.0, 15.0)\n",
    "\n",
    "plt.subplot(131)\n",
    "plt.imshow(input_r.T, interpolation='nearest', cmap=plt.cm.gray)\n",
    "plt.title('Input')\n",
    "plt.subplot(132)\n",
    "plt.imshow(feature_r.T, interpolation='nearest', cmap=plt.cm.gray)\n",
    "plt.title('Feature')\n",
    "plt.subplot(133)\n",
    "plt.imshow(weights.T, interpolation='nearest', cmap=plt.cm.gray)\n",
    "plt.title('Receptive fields')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the file `BarLearning.py`, a visualization class using `pyqtgraph` is\n",
    "imported from `Viz.py`, but the user is free to use whatever method he prefers to\n",
    "visualize the result of learning.\n",
    "\n",
    "```python\n",
    "from Viz import Viewer\n",
    "view = Viewer(func=trial)\n",
    "view.run()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
